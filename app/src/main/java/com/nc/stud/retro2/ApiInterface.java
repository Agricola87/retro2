package com.nc.stud.retro2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("/")
    Call<List<UserInfo>> getUsersInfo();
//    @GET("posts/")
//    Call<List<UserInfo>> getUsersInfo();

    @POST("/command")
    Call<TodoList> createCommand(@Body Command command);
}
