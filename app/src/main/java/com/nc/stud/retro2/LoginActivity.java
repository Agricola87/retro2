package com.nc.stud.retro2;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.security.KeyChain;
import android.security.KeyChainAliasCallback;
import android.security.KeyChainException;
import android.support.v7.app.AppCompatActivity;
import android.webkit.ClientCertRequest;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.security.PrivateKey;
import java.security.cert.X509Certificate;

public class LoginActivity extends AppCompatActivity {

    private WebView myWebView;

    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myWebView = (WebView) findViewById(R.id.webView);
        myWebView.loadUrl("https://192.168.1.100:8443/login");
        myWebView.setWebViewClient(new NocWebViewClient());
//        myWebView.loadUrl("https://google.com/");




    }
    private class NocWebViewClient extends WebViewClient {


        @Override
        public void onReceivedClientCertRequest(WebView view, final ClientCertRequest request) {
            KeyChain.choosePrivateKeyAlias(LoginActivity.this, new KeyChainAliasCallback() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void alias(String alias) {
                    try {
                        PrivateKey changPrivateKey = KeyChain.getPrivateKey(LoginActivity.this, alias);
                        X509Certificate[] certificates = KeyChain.getCertificateChain(LoginActivity.this, alias);
                        request.proceed(changPrivateKey, certificates);
                    } catch (KeyChainException e) {

                    } catch (InterruptedException e) {

                    }
                }
            }, new String[]{"RSA"}, null, null, -1, null);
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            handler.proceed();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            String token = view.getTitle();
            if(url.equals("https://192.168.1.100:8443/main"))
            {
                Intent myIntent = new Intent(getOuter(), ListActivity.class);
                myIntent.putExtra("token", token);
                startActivity(myIntent);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {

        }
    }

    public LoginActivity getOuter() {
        return LoginActivity.this;
    }
}
