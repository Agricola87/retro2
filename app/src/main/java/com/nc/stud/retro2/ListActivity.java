package com.nc.stud.retro2;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class ListActivity extends AppCompatActivity {

    private ApiInterface apiInterface;
    String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        //apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        //apiInterface =

        Intent intent = getIntent();
        token = intent.getStringExtra("token");
        MyListTask mlt = new MyListTask();
        mlt.execute();
    }

    class MyListTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {
//            call = apiInterface.getUsersInfo();
//            try {
//                response = call.execute();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Boolean isSuccessful =
//                    response.isSuccessful();
//            Log.e(TAG, isSuccessful.toString());
            Command command = new Command(
                    "init_list"
//                    , new HashMap<String, String>(){{put("token", "");}}
                    );
            //apiInterface = ApiClient.getApiClient().create(ApiInterface.class);
            ServiceGeneratorWithToken sgwt = new ServiceGeneratorWithToken();
            ApiInterface apiInterface = sgwt.createService(ApiInterface.class, token);
            Call<TodoList> call = apiInterface.createCommand(command);
            try {
                call.execute();
//                TodoList todoList = call.execute().body();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }
}
