package com.nc.stud.retro2;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<UserInfo> usersInfos;

    public RecyclerAdapter(List<UserInfo> usersInfos){
        this.usersInfos = usersInfos;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);

        //rythjrjyrjty
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(usersInfos.get(position).getTitle());
        holder.body.setText(usersInfos.get(position).getBody());

    }

    @Override
    public int getItemCount() {
        return usersInfos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        //Добавить остальные
        TextView title, body;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            body = (TextView)itemView.findViewById(R.id.body);
        }


    }
}
