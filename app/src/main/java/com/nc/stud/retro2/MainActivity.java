package com.nc.stud.retro2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.os.AsyncTask;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "IS Sucessful";

    Call<List<UserInfo>> call = null;

    Response<List<UserInfo>> response = null;

    private RecyclerView recyclerView;

    private RecyclerView.LayoutManager layoutManager;

    private RecyclerAdapter adapter;

    private List<UserInfo> userInfos;

    private ApiInterface apiInterface;

    public void buttonOnClick(View view){
        MyTask mt = new MyTask();
        mt.execute();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);


//        call = apiInterface.getUsersInfo();


//        MyTask mt = new MyTask();
//        mt.execute();



/*
        call.enqueue(new Callback<List<UserInfo>>() {
            @Override
            public void onResponse(Call<List<UserInfo>> call, Response<List<UserInfo>> response) {
                userInfos = response.body();


                adapter = new RecyclerAdapter(userInfos);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<UserInfo>> call, Throwable t) {

            }
        });
        */
    }


    class MyTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected Void doInBackground(Void... params) {
            call = apiInterface.getUsersInfo();
            try {
                response = call.execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Boolean isSuccessful =
                    response.isSuccessful();
            Log.e(TAG, isSuccessful.toString());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

        }
    }
}
