package com.nc.stud.retro2;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.net.ssl.*;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//import static android.content.ContentValues.TAG;

public class ApiClient {
//    public static String BASE_URL = "http://jsonplaceholder.typicode.com/";
public static String BASE_URL = "https://192.168.1.100:8443";

    public static Retrofit retrofit = null;
    private static String TAG = "Text";

    public static Retrofit getApiClient(){
        if(retrofit == null){
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor(
                    new HttpLoggingInterceptor.Logger() {
                @Override public void log(@NonNull String message) {
                    Log.e(TAG, message);
                }
            }).setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            builder.addInterceptor(logging);

            try {
                X509TrustManager trustManager = getTrustManager();
                builder.sslSocketFactory(getSslSocketFactory(trustManager), trustManager);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            OkHttpClient client = builder.build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();


        }
        return retrofit;
    }

    @NonNull
    private static X509TrustManager getTrustManager() {
        return new X509TrustManager() {
            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[] {};
            }
        };
    }
    private static SSLSocketFactory getSslSocketFactory(TrustManager trustManager) throws NoSuchAlgorithmException, KeyManagementException {
        // TLSv1.2 - security team requirement, do not change without approval
        final SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[] {trustManager}, new SecureRandom());
        return sslContext.getSocketFactory();
    }
}
