//package com.nc.stud.retro2.adapter;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ArrayAdapter;
//import android.widget.TextView;
//
//import com.nc.stud.retro2.UserInfo;
//
//import java.util.List;
//
//import com.nc.stud.retro2.R;
//
///**
// * Created by norman on 12/26/16.
// */
//
//public class GitHubRepoAdapter extends ArrayAdapter<UserInfo> {
//
//    private Context context;
//    private List<UserInfo> values;
//
//    public GitHubRepoAdapter(Context context, List<UserInfo> values) {
//        super(context, R.layout.list_item_pagination, values);
//
//        this.context = context;
//        this.values = values;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        View row = convertView;
//
//        if (row == null) {
//            LayoutInflater inflater =
//                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            row = inflater.inflate(R.layout.list_item_pagination, parent, false);
//        }
//
//        TextView textView = (TextView) row.findViewById(R.id.list_item_pagination_text);
//
//        GitHubRepo item = values.get(position);
//        String message = item.getName();
//        textView.setText(message);
//
//        return row;
//    }
//}
